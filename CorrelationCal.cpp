#include <iostream>

#include "CorrelationCal.h"

ts::CorrelationCal::CorrelationCal(ts::TimeSeries &timeSeries1, ts::TimeSeries &timeSeries2)
        : m_timeSeries1(timeSeries1),
          m_timeSeries2(timeSeries2)
{


}

ts::CorrelationCal::CorrelationCal(const ts::CorrelationCal &correlationCal)
        : m_timeSeries1(correlationCal.m_timeSeries1),
          m_timeSeries2(correlationCal.m_timeSeries2)
{

}

ts::CorrelationCal &ts::CorrelationCal::operator=(const ts::CorrelationCal &correlationCal)
{
    if (this != &correlationCal)
    {
        m_timeSeries1 = correlationCal.m_timeSeries1;
        m_timeSeries2 = correlationCal.m_timeSeries2;
    }
    return *this;
}

ts::CorrelationCal::~CorrelationCal()
{

}

double ts::CorrelationCal::getCorrelation()
{
    if (m_timeSeries1.getSize() != m_timeSeries2.getSize()) {
        std::cout << "Error: time series size not equal" << std::endl;
        return -1;
    }

    double sum = 0;
    int size = m_timeSeries1.getSize();
    double mean1 = m_timeSeries1.getAverage();
    double mean2 = m_timeSeries2.getAverage();

    for (int i = 0; i < m_timeSeries1.getSize(); ++i)
    {
        auto val = (m_timeSeries1.getValue(i) - mean1) * (m_timeSeries2.getValue(i) - mean2);
        sum += val;
    }

    double std1 = m_timeSeries1.getStandardDeviation();
    double std2 = m_timeSeries2.getStandardDeviation();
    sum /= (std1 * std2);

    return sum / (size - 1);
}
