#pragma once

#include <vector>
#include "TimeSeries.h"

namespace ts {
    class Filters
    {
    public:
        Filters(TimeSeries &timeSeries, int period);
        Filters(const Filters &filters);
        Filters &operator = (const Filters &filters);
        virtual ~Filters();

        std::vector<double> calMovingAvg();
        std::vector<double> calMovingMed();

    private:
        friend class TimeSeries;

        TimeSeries &m_timeSeries;
        int m_numPeriods;
    };
}
