//
// Created by harshanaj on 11/12/2016.
//

#include <numeric>
#include <cmath>
#include "TimeSeries.h"

ts::TimeSeries::TimeSeries()
        : m_container()
{

}

ts::TimeSeries::TimeSeries(const ts::TimeSeries &timeSeries)
        : m_container(timeSeries.m_container)
{

}

ts::TimeSeries &ts::TimeSeries::operator=(const ts::TimeSeries &timeSeries)
{
    if(this != &timeSeries)
    {
        m_container = timeSeries.m_container;
    }
    return *this;
}

ts::TimeSeries::~TimeSeries()
{

}

void ts::TimeSeries::add(int time, int value)
{
    m_container[time] = value;
}

int ts::TimeSeries::getSize()
{
    return m_container.size();
}

int ts::TimeSeries::getValue(int time)
{
    auto itr = m_container.find(time);
    if(itr != m_container.end())
    {
        return (*itr).second;
    }
    return INT32_MIN;
}

double ts::TimeSeries::getAverage()
{
    int sum = 0;
    sum = std::accumulate(m_container.begin(), m_container.end(), 0,
                          [] (int value, const std::map<int, int>::value_type& p)
                          { return value + p.second; });
    return sum/getSize();
}

double ts::TimeSeries::getVariance()
{
    int sum = 0;
    double mean = getAverage();
    sum = std::accumulate(m_container.begin(), m_container.end(), 0,
                          [mean] (int value, const std::map<int, int>::value_type& p)
                          { return value + ((p.second - mean) * (p.second - mean)); });
    return sum/m_container.size();
}

double ts::TimeSeries::getStandardDeviation()
{
    double variance = getVariance();
    return std::sqrt(variance / (m_container.size()-1));
}

