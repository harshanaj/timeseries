//
// Created by harshanaj on 11/12/2016.
//

#pragma once

#include <map>

namespace ts
{
    class TimeSeries
    {
    public:
        TimeSeries();
        TimeSeries(const TimeSeries &timeSeries);
        TimeSeries &operator=(const TimeSeries &timeSeries);
        virtual ~TimeSeries();

        void add(int time, int value);
        int getSize();
        int getValue(int time);
        double getAverage();
        double getVariance();
        double getStandardDeviation();

    protected:
        std::map<int, int> m_container;
    };
}

